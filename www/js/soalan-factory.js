var app  = angular.module('app.services', [])

app.factory('soalanAFactory', [function() {
    var marks = [];
    return {
        setData: function(data) {
            marks = data;
        },
        getData: function(index) {
            return marks[index];
        },
        getAllData: function() {
            return marks;
        }
    }
}])

app.factory('soalanBFactory', [function() {
    var marks = [];
    return {
        setData: function(data) {
            marks = data;
        },
        getData: function() {
            return marks;
        }
    }
}])

app.factory('soalanCFactory', [function() {
    var marks = [];
    return {
        setData: function(data) {
            marks = data;
        },
        getData: function() {
            return marks;
        }
    }
}])

app.factory('soalanDFactory', [function() {
    var marks = [];
    return {
        setData: function(data) {
            marks = data;
        },
        getData: function() {
            return marks;
        }
    }
}])

app.factory('soalanEFactory', [function() {
    var marks = [];
    return {
        setData: function(data) {
            marks = data;
        },
        getData: function() {
            return marks;
        }
    }
}])

app.factory('soalanFFactory', [function() {
    var marks = [];
    return {
        setData: function(data) {
            marks = data;
        },
        getData: function() {
            return marks;
        }
    }
}])

app.factory('soalanGFactory', [function() {
    var marks = [];
    return {
        setData: function(data) {
            marks = data;
        },
        getData: function() {
            return marks;
        }
    }
}])


app.factory('responseAFactory', [function() {
    var marks = [];
    return {
        setData: function(data) {
            marks = data;
        },
        getSpecData: function(question) {
            var specResponse = [];
            for(var i = 0; i < marks.length; i++){
                if(marks[i].question_id == question){
                    specResponse.push(marks[i]);
                }
            }
            return specResponse;
        },
        getAllData: function() {
            return marks;
        },
        getAnswer: function(question_id, id){
            for(var i = 0; i < marks.length; i++){
                if(marks[i].question_id == question_id && marks[i].id == id){
                    return marks[i];
                }
            }

            return null;
        }
    }
}])

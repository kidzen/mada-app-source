app.factory('sessionService', ['$http', function($http) {
  return {
    set: function(key, value) {
      return localStorage.setItem(key, JSON.stringify(value));
    },
    get: function(key) {
      return JSON.parse(localStorage.getItem(key));
    },
    destroy: function(key) {
      return localStorage.removeItem(key);
    },
  };
}])

app.factory('localForageFactory', function($localForage, $q) {
  return {
    set: function(key, value) {
      return $localForage.setItem(key, value);
    },
    get: function(key) {
      return $localForage.getItem(key);
    },
    destroy: function() {
      $localForage.clear();
    }
  };
})

app.factory('homeValue', function($http, $q, $ionicLoading) {
  var d = $q.defer();
  var homeVal = {};
  return {
    set: function(value) {
      d.resolve(value);
    },
    get: function() {
      return d.promise;
    }
  };
})

app.factory('homeValueInit', function($http, $q, $ionicLoading) {
  var homeVal = {};
  return {
    set: function(value) {
      homeVal = value;
    },
    get: function() {
      return homeVal;
    }
  };
})

app.factory('soalanLength', function(localForageFactory, $q, $ionicLoading) {
  var d = $q.defer();
  var length = 0;
  return {
    set: function(value) {
      length = value;
    },
    get: function() {
      return length;
    }
  };
})

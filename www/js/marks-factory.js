app.factory('marksAFactory', function($state) {
  var marks = [];
  var sendMarks = [];
  return {
    setData: function(data) {
      var bool = true;
      if (marks.length == 0) {
        marks.push({ ques: data.ques, quesId: data.quesId, ans: data.ans, ansId: data.ansId });
        sendMarks.push({ quesId: data.quesId, ans: data.ansId });
        bool = false;
      } else {
        for (i = 0; i < marks.length; i++) {
          if (marks[i].ques == data.ques) {
            marks[i].ans = data.ans;
            bool = false;
          }
        }
      }

      if (bool) {
        marks.push({ ques: data.ques, quesId: data.quesId, ans: data.ans, ansId: data.ansId });
        sendMarks.push({ quesId: data.quesId, ans: data.ansId });
      }

      if (data.go) {
        $state.go(data.go);
      }
    },
    getData: function() {
      return marks;
    },
    getSendData: function() {
      return sendMarks;
    },
    getAnswer: function(id){
      for(var i = 0; i < marks.length; i++){
        if(marks[i].quesId == id){
          return marks[i].ansId;
        }
      }

      return null;
    },
    clearData: function() {
      marks = [];
      sendMarks = [];
    }
  }
})

app.factory('marksBFactory', [function() {
  var marks = [];
  var sendMarks = [];
  return {
    setData: function(data, ques, name) {
      var bool = true;
      if (sendMarks.length == 0) {
        sendMarks.push({ name:name, quesId: ques, ans: data});
        bool = false;
      } else {
        for (i = 0; i < sendMarks.length; i++) {
          if (sendMarks[i].quesId == ques) {
            sendMarks[i].ans = data;
            bool = false;
          }
        }
      }

      if (bool) {
        sendMarks.push({ name:name, quesId: ques, ans: data });
      }
    },
    getData: function(id) {
      var rVal;
      for (var i = 0; i < sendMarks.length; i++) {
        if (sendMarks[i].quesId == id) {
          rVal = sendMarks[i];
        }
      }
      if (rVal) {
        return rVal.ans;
      } else {
        return null;
      }
    },
    getSendData: function() {
      return sendMarks;
    },
    clearData: function() {
      marks = [];
      sendMarks = [];
    }
  }
}])

app.factory('marksCFactory', [function() {
  var marks = [];
  var sendMarks = [];
  return {
    setData: function(data, ques, name) {
      var bool = true;
      if (sendMarks.length == 0) {
        sendMarks.push({ name:name, quesId: ques, ans: data });
        bool = false;
      } else {
        for (i = 0; i < sendMarks.length; i++) {
          if (sendMarks[i].quesId == ques) {
            sendMarks[i].ans = data;
            bool = false;
          }
        }
      }

      if (bool) {
        sendMarks.push({ name:name, quesId: ques, ans: data });
      }
      
    },
    getData: function(id) {
      var rVal;
      for (var i = 0; i < sendMarks.length; i++) {
        if (sendMarks[i].quesId == id) {
          rVal = sendMarks[i];
        }
      }
      if (rVal) {
        return rVal.ans;
      } else {
        return null;
      }
    },
    getSendData: function() {
      return sendMarks;
    },
    clearData: function() {
      marks = [];
      sendMarks = [];
    }
  }
}])

app.factory('marksDFactory', [function() {
  var marks = [];
  var sendMarks = [];
  return {
    setData: function(data, ques, name) {
      var bool = true;
      if (sendMarks.length == 0) {
        sendMarks.push({ name:name, quesId: ques, ans: data });
        bool = false;
      } else {
        for (i = 0; i < sendMarks.length; i++) {
          if (sendMarks[i].quesId == ques) {
            sendMarks[i].ans = data;
            bool = false;
          }
        }
      }

      if (bool) {
        sendMarks.push({ name:name, quesId: ques, ans: data });
      }

      
    },
    getData: function(id) {
      var rVal;
      for (var i = 0; i < sendMarks.length; i++) {
        if (sendMarks[i].quesId == id) {
          rVal = sendMarks[i];
        }
      }
      if (rVal) {
        return rVal.ans;
      } else {
        return null;
      }
    },
    getSendData: function() {
      return sendMarks;
    },
    clearData: function() {
      marks = [];
      sendMarks = [];
    }
  }
}])

app.factory('marksEFactory', [function() {
  var marks = [];
  var sendMarks = [];
  return {
    setData: function(data, ques, name) {
      var bool = true;
      if (sendMarks.length == 0) {
        sendMarks.push({ name:name, quesId: ques, ans: data });
        bool = false;
      } else {
        for (i = 0; i < sendMarks.length; i++) {
          if (sendMarks[i].quesId == ques) {
            sendMarks[i].ans = data;
            bool = false;
          }
        }
      }

      if (bool) {
        sendMarks.push({ name:name, quesId: ques, ans: data });
      }

      
    },
    getData: function(id) {
      var rVal;
      for (var i = 0; i < sendMarks.length; i++) {
        if (sendMarks[i].quesId == id) {
          rVal = sendMarks[i];
        }
      }
      if (rVal) {
        return rVal.ans;
      } else {
        return null;
      }
    },
    getSendData: function() {
      return sendMarks;
    },
    clearData: function() {
      marks = [];
      sendMarks = [];
    }
  }
}])

app.factory('marksFFactory', [function() {
  var marks = [];
  var sendMarks = [];
  return {
    setData: function(data, ques, name) {
      var bool = true;
      if (sendMarks.length == 0) {
        sendMarks.push({ name:name, quesId: ques, ans: data });
        bool = false;
      } else {
        for (i = 0; i < sendMarks.length; i++) {
          if (sendMarks[i].quesId == ques) {
            sendMarks[i].ans = data;
            bool = false;
          }
        }
      }

      if (bool) {
        sendMarks.push({ name:name, quesId: ques, ans: data });
      }

      
    },
    getData: function(id) {
      var rVal;
      for (var i = 0; i < sendMarks.length; i++) {
        if (sendMarks[i].quesId == id) {
          rVal = sendMarks[i];
        }
      }
      if (rVal) {
        return rVal.ans;
      } else {
        return null;
      }
    },
    getSendData: function() {
      return sendMarks;
    },
    clearData: function() {
      marks = [];
      sendMarks = [];
    }
  }
}])

app.factory('marksGFactory', [function() {
  var marks = "";
  var sendMarks = [];
  return {
    setData: function(data, quesId, name) {
      marks = data;
      sendMarks = [{ name:name, quesId: quesId, ans: data }];
    },
    getData: function() {
      return marks;
    },
    getSendData: function() {
      return sendMarks;
    },
    clearData: function() {
      marks = "";
      sendMarks = [];
    }
  }
}])

angular.module('app.controllers', [])

  .controller('homeCtrl',
    function($scope, $stateParams, $http, API_URL, httpService, $state, $http, sessionService, homeValue, $ionicLoading, localForageFactory, homeValueInit,
      marksAFactory, marksBFactory, marksCFactory, marksDFactory, marksEFactory, marksFFactory, marksGFactory, soalanAFactory, $localForage, $rootScope, $window) {

      $rootScope.$on("homeMethod", function() {
        $scope.reinit();
      });

      $rootScope.$on("booleanHome", function() {
        $scope.bool = true;
        var value = { button: 'Mula', text: 'Sila Pastikan Jawab Kesemua Soalan Sebelum Keluar Dari Applikasi MADA' };
        $scope.text = value;
      });




      $scope.init = function() {
        $scope.bool = true;
        homeValue.get().then(
          function(res) {
            $scope.text = res;
          },
          function(err) {
            console.error(err);
          }
        );
      }

      $scope.reinit = function() {
        $scope.bool = true;
        var value = { button: 'Lihat Statistik', text: 'Bancian Anda Telah Diterima dan Direkod. Untuk Melihat Bancian, Sila Sentuh "Lihat Statistik"' };
        $scope.text = value;
      }

      $scope.clear = function() {
        // $http({
        //   method: 'GET',
        //   url: 'http://www.rekamy.com/estor/api/web/v1/inventory-items/010901020916000025',
        //   headers: { 'Content-Type': 'application/json' }
        // }).success(function(response) {
        //   console.log(response);
        // }).error(function(err) {
        //   console.log(err);
        // })
        localForageFactory.set("id", null);
        localForageFactory.set("response", null);
        alert('Data Cleared');
      }



      $scope.start = function() {
        if ($scope.bool) {
          $scope.bool = !$scope.bool;
          localForageFactory.get("id").then(
            function(id) {
              //console.log(id);
              if (!id) {
                var url = "http://freegeoip.net/json/";
                $http.get(url).then(function(response) {
                  var data = { ip: response.data.ip };
                  $http({
                    method: 'POST',
                    url: API_URL + "/getId",
                    data: data,
                    headers: { 'Content-Type': 'application/json' }
                  }).success(function(res) {
                    localForageFactory.set("id", res);
                    $state.go("bhgA1");
                  }).error(function(err) {
                    console.log(err);
                  })

                });
              } else {
                //jump to unanswered page
                var marksType = ['A', 'B', 'C', 'D', 'E', 'F', 'G'];
                for (var i = 0; i < marksType.length; i++) {
                  if ($scope.checkData(marksType[i])) {
                    $state.go($scope.checkData(marksType[i]));
                    break;
                  }
                }
              }
            },
            function(err) {
              console.error(err);
            }
          );
        }
      }

      $scope.checkData = function(marksType) {
        switch (marksType) {
          case 'A':
            for (var i = 0; i < soalanAFactory.getAllData().length; i++) {
              if (!marksAFactory.getSendData()[i]) {
                return 'bhgA' + (i + 1);
              }
            }

            return false;
            break;
          case 'B':
            return marksBFactory.getSendData().length ? false : 'bhgB';
            break;
          case 'C':
            return marksCFactory.getSendData().length ? false : 'bhgC';
            break;
          case 'D':
            return marksDFactory.getSendData().length ? false : 'bhgD';
            break;
          case 'E':
            return marksEFactory.getSendData().length ? false : 'bhgE';
            break;
          case 'F':
            return marksFFactory.getSendData().length ? false : 'bhgF';
            break;
          case 'G':
            return marksGFactory.getSendData().length ? 'chart' : 'bhgG';
            break;
          default:
            return null;
            break;
        }
      }

      $scope.init();

    })

  .controller('bhgA1Ctrl', function($scope, $stateParams, $state, marksAFactory, $http, API_URL, soalanAFactory, responseAFactory, httpService) {
    $scope.topic = soalanAFactory.getData(0);
    $scope.ques = responseAFactory.getSpecData($scope.topic.id);
    $scope.buttonClass = "bar bar-footer disabled";


    $scope.enabled = function() {
      $scope.buttonClass = "bar bar-footer your-sample-class";
    }

    $scope.a1 = {};
    //set answer
    $scope.a1.tempat = responseAFactory.getAnswer($scope.topic.id, marksAFactory.getAnswer($scope.topic.id));
    $scope.savebhgA = function() {
      if (!$scope.buttonClass.includes("disabled")) {
        $scope.set = {
          ques: $scope.a1.tempat.question,
          quesId: $scope.a1.tempat.question_id,
          ans: $scope.a1.tempat.response,
          ansId: $scope.a1.tempat.id,
          go: "bhgA2"
        };
        $scope.response = [{
          name: $scope.topic.name,
          question_id: $scope.a1.tempat.question_id,
          question: $scope.a1.tempat.question,
          response: $scope.a1.tempat.response,
          response_id: $scope.a1.tempat.id,
          quesId: $scope.a1.tempat.question_id,
          ans: $scope.a1.tempat.id
        }];
        //$scope.response = [{ quesId: $scope.a1.tempat.question_id, ans: $scope.a1.tempat.id }];
        httpService.sendResponse($scope.response);
        marksAFactory.setData($scope.set);
      }
    }
  })

  .controller('bhgA2Ctrl', function($scope, $stateParams, marksAFactory, soalanAFactory, responseAFactory, httpService) {
    $scope.topic = soalanAFactory.getData(1);
    $scope.ques = responseAFactory.getSpecData($scope.topic.id);
    $scope.buttonClass = "bar bar-footer disabled";

    $scope.enabled = function() {
      $scope.buttonClass = "bar bar-footer your-sample-class";
    }

    $scope.a1 = {};
    $scope.a1.tempat = responseAFactory.getAnswer($scope.topic.id, marksAFactory.getAnswer($scope.topic.id));
    $scope.savebhgA = function() {
      if (!$scope.buttonClass.includes("disabled")) {
        $scope.set = {
          ques: $scope.a1.tempat.question,
          quesId: $scope.a1.tempat.question_id,
          ans: $scope.a1.tempat.response,
          ansId: $scope.a1.tempat.id,
          go: "bhgA3"
        };
        //$scope.response = [{ quesId: $scope.a1.tempat.question_id, ans: $scope.a1.tempat.id }];
        $scope.response = [{
          name: $scope.topic.name,
          question_id: $scope.a1.tempat.question_id,
          question: $scope.a1.tempat.question,
          response: $scope.a1.tempat.response,
          response_id: $scope.a1.tempat.id,
          quesId: $scope.a1.tempat.question_id,
          ans: $scope.a1.tempat.id
        }];
        httpService.sendResponse($scope.response);
        marksAFactory.setData($scope.set);
      }
    }
  })

  .controller('bhgA3Ctrl', function($scope, $stateParams, marksAFactory, soalanAFactory, responseAFactory, httpService) {
    $scope.topic = soalanAFactory.getData(2);
    $scope.ques = responseAFactory.getSpecData($scope.topic.id);

    for (var i = 0; i < $scope.ques.length; i++) {
      var splitString = $scope.ques[i].response.split("(");
      $scope.ques[i].desc = splitString[0];
      $scope.ques[i].gred = "(" + splitString[1];
    }

    $scope.buttonClass = "bar bar-footer disabled";

    $scope.enabled = function() {
      $scope.buttonClass = "bar bar-footer your-sample-class";
    }
    $scope.a1 = {};
    $scope.a1.tempat = responseAFactory.getAnswer($scope.topic.id, marksAFactory.getAnswer($scope.topic.id));
    $scope.savebhgA = function() {
      if (!$scope.buttonClass.includes("disabled")) {
        $scope.set = {
          ques: $scope.a1.tempat.question,
          quesId: $scope.a1.tempat.question_id,
          ans: $scope.a1.tempat.response,
          ansId: $scope.a1.tempat.id,
          go: "bhgA4"
        };
        //$scope.response = [{ quesId: $scope.a1.tempat.question_id, ans: $scope.a1.tempat.id }];
        $scope.response = [{
          name: $scope.topic.name,
          question_id: $scope.a1.tempat.question_id,
          question: $scope.a1.tempat.question,
          response: $scope.a1.tempat.response,
          response_id: $scope.a1.tempat.id,
          quesId: $scope.a1.tempat.question_id,
          ans: $scope.a1.tempat.id
        }];
        httpService.sendResponse($scope.response);
        marksAFactory.setData($scope.set);
      }
    }
  })

  .controller('bhgA4Ctrl', function($scope, $stateParams, marksAFactory, soalanAFactory, responseAFactory, httpService) {
    $scope.topic = soalanAFactory.getData(3);
    $scope.ques = responseAFactory.getSpecData($scope.topic.id);

    $scope.buttonClass = "bar bar-footer disabled";

    $scope.enabled = function() {
      $scope.buttonClass = "bar bar-footer your-sample-class";
    }

    $scope.a1 = {};
    $scope.a1.tempat = responseAFactory.getAnswer($scope.topic.id, marksAFactory.getAnswer($scope.topic.id));
    $scope.savebhgA = function() {
      if (!$scope.buttonClass.includes("disabled")) {
        $scope.set = {
          ques: $scope.a1.tempat.question,
          quesId: $scope.a1.tempat.question_id,
          ans: $scope.a1.tempat.response,
          ansId: $scope.a1.tempat.id,
          go: "bhgA5"
        };
        //$scope.response = [{ quesId: $scope.a1.tempat.question_id, ans: $scope.a1.tempat.id }];
        $scope.response = [{
          name: $scope.topic.name,
          question_id: $scope.a1.tempat.question_id,
          question: $scope.a1.tempat.question,
          response: $scope.a1.tempat.response,
          response_id: $scope.a1.tempat.id,
          quesId: $scope.a1.tempat.question_id,
          ans: $scope.a1.tempat.id
        }];
        httpService.sendResponse($scope.response);
        marksAFactory.setData($scope.set);
      }
    }

  })

  .controller('bhgA5Ctrl', function($scope, $stateParams, marksAFactory, soalanAFactory, responseAFactory, httpService) {
    $scope.topic = soalanAFactory.getData(4);
    $scope.ques = responseAFactory.getSpecData($scope.topic.id);

    $scope.buttonClass = "bar bar-footer disabled";

    $scope.enabled = function() {
      $scope.buttonClass = "bar bar-footer your-sample-class";
    }

    $scope.a1 = {};
    $scope.a1.tempat = responseAFactory.getAnswer($scope.topic.id, marksAFactory.getAnswer($scope.topic.id));
    $scope.savebhgA = function() {
      if (!$scope.buttonClass.includes("disabled")) {
        $scope.set = {
          ques: $scope.a1.tempat.question,
          quesId: $scope.a1.tempat.question_id,
          ans: $scope.a1.tempat.response,
          ansId: $scope.a1.tempat.id,
          go: "bhgB"
        };
        //$scope.response = [{ quesId: $scope.a1.tempat.question_id, ans: $scope.a1.tempat.id }];
        $scope.response = [{
          name: $scope.topic.name,
          question_id: $scope.a1.tempat.question_id,
          question: $scope.a1.tempat.question,
          response: $scope.a1.tempat.response,
          response_id: $scope.a1.tempat.id,
          quesId: $scope.a1.tempat.question_id,
          ans: $scope.a1.tempat.id
        }];
        httpService.sendResponse($scope.response);
        marksAFactory.setData($scope.set);
      }
    }

  })

  .controller('bhgBCtrl',
    function($scope, $stateParams, $ionicPopup, madaService, marksBFactory, soalanBFactory, $state, httpService) {
      $scope.temp = soalanBFactory.getData();
      $scope.buttonClass = "bar bar-footer disabled";

      $scope.enabled = function() {
        $scope.buttonClass = "bar bar-footer your-sample-class";
      }

      $scope.soalan = function() {
        $scope.kenyataan = [];
        for (var i = 0; i < $scope.temp.length; i++) {
          $scope.kenyataan.push({ name: $scope.temp[i].name, soalan: $scope.temp[i].question, id: $scope.temp[i].id, index: i, mark: madaService.getAnswerMark(marksBFactory.getData($scope.temp[i].id)) })
        }
      }

      var myPopup;
      $scope.marks = function(data) {
        $scope.data = {};
        $scope.data.val = marksBFactory.getData(data.id);
        $scope.data.id = data.id;
        $scope.data.name = data.name;
        var customTemplate = madaService.customTemplate();

        myPopup = $ionicPopup.show({
          template: customTemplate,
          title: 'Skor',
          scope: $scope
        });
      }

      $scope.save = function() {
        marksBFactory.setData($scope.data.val, $scope.data.id, $scope.data.name);
        $scope.soalan();
        if (marksBFactory.getSendData().length == $scope.temp.length) {
          $scope.enabled();
        }
        myPopup.close();
      }

      $scope.next = function() {
        if (!$scope.buttonClass.includes("disabled")) {
          httpService.sendResponse(marksBFactory.getSendData(), "bhgC");
        }
      }

      $scope.soalan();

    }
  )

  .controller('bhgCCtrl',
    function($scope, $stateParams, $ionicPopup, madaService, marksCFactory, soalanCFactory, httpService) {
      $scope.temp = soalanCFactory.getData();

      $scope.buttonClass = "bar bar-footer disabled";

      $scope.enabled = function() {
        $scope.buttonClass = "bar bar-footer your-sample-class";
      }

      $scope.soalan = function() {
        $scope.kenyataan = [];
        for (var i = 0; i < $scope.temp.length; i++) {
          $scope.kenyataan.push({ name: $scope.temp[i].name, soalan: $scope.temp[i].question, id: $scope.temp[i].id, index: i, mark: madaService.getAnswerMark(marksCFactory.getData($scope.temp[i].id)) })
        }
      }

      var myPopup;
      $scope.marks = function(data) {
        $scope.data = {};
        $scope.data.val = marksCFactory.getData(data.id);
        $scope.data.id = data.id;
        $scope.data.name = data.name;
        var customTemplate = madaService.customTemplate();

        myPopup = $ionicPopup.show({
          template: customTemplate,
          title: 'Skor',
          scope: $scope
        });
      }

      $scope.save = function() {
        marksCFactory.setData($scope.data.val, $scope.data.id, $scope.data.name);
        $scope.soalan();
        if (marksCFactory.getSendData().length == $scope.temp.length) {
          $scope.enabled();
        }
        myPopup.close();
      }


      $scope.next = function() {
        if (!$scope.buttonClass.includes("disabled")) {
          httpService.sendResponse(marksCFactory.getSendData(), "bhgD");
        }
      }

      $scope.soalan();

    }
  )

  .controller('bhgDCtrl',
    function($scope, $stateParams, $ionicPopup, madaService, marksDFactory, soalanDFactory, httpService) {
      $scope.temp = soalanDFactory.getData();

      $scope.buttonClass = "bar bar-footer disabled";

      $scope.enabled = function() {
        $scope.buttonClass = "bar bar-footer your-sample-class";
      }
      $scope.soalan = function() {
        $scope.kenyataan = [];
        for (var i = 0; i < $scope.temp.length; i++) {
          $scope.kenyataan.push({ name: $scope.temp[i].name, soalan: $scope.temp[i].question, id: $scope.temp[i].id, index: i, mark: madaService.getAnswerMark(marksDFactory.getData($scope.temp[i].id)) })
        }
      }

      var myPopup;
      $scope.marks = function(data) {
        $scope.data = {};
        $scope.data.val = marksDFactory.getData(data.id);
        $scope.data.id = data.id;
        $scope.data.name = data.name;
        var customTemplate = madaService.customTemplate();

        myPopup = $ionicPopup.show({
          template: customTemplate,
          title: 'Skor',
          scope: $scope
        });
      }

      $scope.save = function() {
        marksDFactory.setData($scope.data.val, $scope.data.id, $scope.data.name);
        $scope.soalan();
        if (marksDFactory.getSendData().length == $scope.temp.length) {
          $scope.enabled();
        }
        myPopup.close();
      }

      $scope.next = function() {
        if (!$scope.buttonClass.includes("disabled")) {
          httpService.sendResponse(marksDFactory.getSendData(), "bhgE");
        }
      }

      $scope.soalan();

    }
  )

  .controller('bhgECtrl',
    function($scope, $stateParams, $ionicPopup, madaService, marksEFactory, soalanEFactory, httpService) {
      $scope.temp = soalanEFactory.getData();

      $scope.buttonClass = "bar bar-footer disabled";

      $scope.enabled = function() {
        $scope.buttonClass = "bar bar-footer your-sample-class";
      }
      $scope.soalan = function() {
        $scope.kenyataan = [];
        for (var i = 0; i < $scope.temp.length; i++) {
          $scope.kenyataan.push({ name: $scope.temp[i].name, soalan: $scope.temp[i].question, id: $scope.temp[i].id, index: i, mark: madaService.getAnswerMark(marksEFactory.getData($scope.temp[i].id)) })
        }
      }

      var myPopup;
      $scope.marks = function(data) {
        $scope.data = {};
        $scope.data.val = marksEFactory.getData(data.id);
        $scope.data.id = data.id;
        $scope.data.name = data.name;
        var customTemplate = madaService.customTemplate();

        myPopup = $ionicPopup.show({
          template: customTemplate,
          title: 'Skor',
          scope: $scope
        });
      }

      $scope.save = function() {
        marksEFactory.setData($scope.data.val, $scope.data.id, $scope.data.name);
        $scope.soalan();
        if (marksEFactory.getSendData().length == $scope.temp.length) {
          $scope.enabled();
        }
        myPopup.close();
      }

      $scope.next = function() {
        if (!$scope.buttonClass.includes("disabled")) {
          httpService.sendResponse(marksEFactory.getSendData(), "bhgF");
        }
      }

      $scope.soalan();

    }
  )

  .controller('bhgFCtrl',
    function($scope, $stateParams, $ionicPopup, madaService, marksFFactory, soalanFFactory, httpService) {
      $scope.temp = soalanFFactory.getData();

      $scope.buttonClass = "bar bar-footer disabled";

      $scope.enabled = function() {
        $scope.buttonClass = "bar bar-footer your-sample-class";
      }
      $scope.soalan = function() {
        $scope.kenyataan = [];
        for (var i = 0; i < $scope.temp.length; i++) {
          $scope.kenyataan.push({ name: $scope.temp[i].name, soalan: $scope.temp[i].question, id: $scope.temp[i].id, index: i, mark: madaService.getAnswerMark(marksFFactory.getData($scope.temp[i].id)) })
        }
      }

      var myPopup;
      $scope.marks = function(data) {
        $scope.data = {};
        $scope.data.val = marksFFactory.getData(data.id);
        $scope.data.id = data.id;
        $scope.data.name = data.name;
        var customTemplate = madaService.customTemplate();

        myPopup = $ionicPopup.show({
          template: customTemplate,
          title: 'Skor',
          scope: $scope
        });
      }

      $scope.save = function() {
        marksFFactory.setData($scope.data.val, $scope.data.id, $scope.data.name);
        $scope.soalan();
        if (marksFFactory.getSendData().length == $scope.temp.length) {
          $scope.enabled();
        }
        myPopup.close();
      }


      $scope.next = function() {
        if (!$scope.buttonClass.includes("disabled")) {
          httpService.sendResponse(marksFFactory.getSendData(), "bhgG");
        }
      }

      $scope.soalan();

    }
  )

  .controller('bhgGCtrl',
    function($scope, $stateParams, marksGFactory, soalanGFactory, httpService) {
      $scope.bhgG = {};
      $scope.bhgG.soalan = soalanGFactory.getData();

      $scope.buttonClass = "bar bar-footer  disabled";

      $scope.enabled = function(length) {
        if (length) {
          $scope.buttonClass = "bar bar-footer your-sample-class";
        } else {
          $scope.buttonClass = "bar bar-footer disabled";
        }

      }

      $scope.saveBhgG = function() {
        if (!$scope.buttonClass.includes("disabled")) {
          marksGFactory.setData($scope.bhgG.desc, $scope.bhgG.soalan[0].id, $scope.bhgG.soalan[0].name);
          httpService.sendResponse(marksGFactory.getSendData(), "summary");
        }
      }

    }
  )

  .controller('summaryCtrl',
    function($state, $scope, $stateParams, marksAFactory, marksBFactory, marksCFactory, marksDFactory, marksEFactory, marksFFactory, marksGFactory, $ionicPopup, homeValue, $rootScope, homeValueInit) {

      $scope.clearData = function() {
        $rootScope.$emit("homeMethod");
        $state.go("home");
      }
    }
  )

  .controller('chartCtrl', function($scope, $stateParams, $timeout, marksAFactory, marksBFactory,
    marksCFactory, marksDFactory, marksEFactory, marksFFactory, marksGFactory, $ionicPopup) {
    $scope.select = {};
    $scope.select.bhg = "A";



    $scope.bahagian = [
      { bhg: "Bahagian A: Maklumat Asas Responden", type: 'A', selected: true },
      { bhg: "Bahagian B: Pengurusan Organisasi", type: 'B', selected: false },
      { bhg: "Bahagian C: Kepuasan Kerja", type: 'C', selected: false },
      { bhg: "Bahagian D: Kelengkapan Pejabat", type: 'D', selected: false },
      { bhg: "Bahagian E: Tekanan Di Tempat Kerja", type: 'E', selected: false },
      { bhg: "Bahagian F: Kemudahan - Kemudahan Lain", type: 'F', selected: false },
      { bhg: "Bahagian G: Pandangan Dan Cadangan", type: 'G', selected: false }
    ];

    $scope.popup = function() {
      $scope.data = {};
      var customTemplate =
        '<ion-radio class="item-text-wrap" ng-value="A" ng-click="chartView(' + "'A'" + ')">A: Maklumat Asas Responden</ion-radio>' +
        '<ion-radio class="item-text-wrap" ng-value="B" ng-click="chartView(' + "'B'" + ')">B: Pengurusan Organisasi</ion-radio>' +
        '<ion-radio class="item-text-wrap" ng-value="C" ng-click="chartView(' + "'C'" + ')">C: Kepuasan Kerja</ion-radio>' +
        '<ion-radio class="item-text-wrap" ng-value="D" ng-click="chartView(' + "'D'" + ')">D: Kelengkapan Pejabat</ion-radio>' +
        '<ion-radio class="item-text-wrap" ng-value="E" ng-click="chartView(' + "'E'" + ')">E: Tekanan Di Tempat Kerja</ion-radio>' +
        '<ion-radio class="item-text-wrap" ng-value="F" ng-click="chartView(' + "'F'" + ')">F: Kemudahan - Kemudahan Lain</ion-radio>' +
        '<ion-radio class="item-text-wrap" ng-value="G" ng-click="chartView(' + "'G'" + ')">G: Pandangan Dan Cadangan</ion-radio>';

      $scope.myPopup = $ionicPopup.show({
        template: customTemplate,
        title: 'Bahagian',
        scope: $scope
      });
    }


    $scope.chartView = function(data, con) {
      if (!con) {
        $scope.myPopup.close();
      }

      switch (data) {
        case 'A':
          $scope.dataFactory = marksAFactory.getData();
          break;
        case 'B':
          $scope.dataFactory = marksBFactory.getSendData();
          break;
        case 'C':
          $scope.dataFactory = marksCFactory.getSendData();
          break;
        case 'D':
          $scope.dataFactory = marksDFactory.getSendData();
          break;
        case 'E':
          $scope.dataFactory = marksEFactory.getSendData();
          break;
        case 'F':
          $scope.dataFactory = marksFFactory.getSendData();
          break;
        case 'G':
          $scope.dataFactory = marksGFactory.getData();
          break;
      }

      switch (data) {
        case 'A':
          for (var i = 0; i < $scope.dataFactory.length; i++) {
            if ($scope.dataFactory[i].ques == "Kumpulan Gred") {
              console.log($scope.dataFactory[i].ans);
              var splitString = $scope.dataFactory[i].ans.split("(");
              $scope.dataFactory[i].ans = splitString[0] + "\n(" + splitString[1];
            }
          }

          $scope.bahagianSoalan = $scope.bahagian[0].bhg;
          console.log($scope.bahagianSoalan);
          $scope.showBhgA = true;
          $scope.showChart = false;
          $scope.showBhgG = false;
          break
        case 'G':
          $scope.bahagianSoalan = $scope.bahagian[6].bhg;
          $scope.showBhgG = true;
          $scope.showBhgA = false;
          $scope.showChart = false;
          break
        default:
          $scope.showChart = true;
          $scope.showBhgG = false;
          $scope.showBhgA = false;
          var percent, remainder;
          var total = 0;
          $timeout(function() {
            for (i = 0; i < $scope.dataFactory.length; i++) {
              total = total + parseInt($scope.dataFactory[i].ans);
            }

            angular.forEach($scope.bahagian, function(value, key) {
              if (value.type == data) {
                $scope.bahagianSoalan = value.bhg;
              }
            });
            percent = ((total / 25) * 100);
            percent = parseInt(percent);
            $scope.peratus = percent;

            remainder = (100 - percent);
            $scope.labels = ["Peratusan Kepuasan", "Peratusan Ketidakpuasan"];
            $scope.data = [percent, remainder];
            $scope.colours = ["#3176d6", "#ff3333"]

          })
          break;
      }
    }


    $scope.chartView('A', true);
  })

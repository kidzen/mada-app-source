app.service('madaService', ['$ionicPopup', 'soalanAFactory', 'soalanBFactory', 'soalanCFactory', 'soalanDFactory', 'soalanEFactory', 'soalanFFactory', 'soalanGFactory', 'responseAFactory',
  'marksAFactory', 'marksBFactory', 'marksCFactory', 'marksDFactory', 'marksEFactory', 'marksFFactory', 'marksGFactory', 'homeValue',
  function($ionicPopup, soalanAFactory, soalanBFactory, soalanCFactory, soalanDFactory, soalanEFactory, soalanFFactory, soalanGFactory, responseAFactory,
    marksAFactory, marksBFactory, marksCFactory, marksDFactory, marksEFactory, marksFFactory, marksGFactory, homeValue) {
    var session = this;
    this.customTemplate = function() {
      var customTemplate =
        '<ion-radio ng-value=1 ng-click="save()" ng-model="data.val">Amat Tidak Memuaskan</ion-radio>' +
        '<ion-radio ng-value=2 ng-click="save()" ng-model="data.val">Tidak Memuaskan</ion-radio>' +
        '<ion-radio ng-value=3 ng-click="save()" ng-model="data.val">Tidak Pasti</ion-radio>' +
        '<ion-radio ng-value=4 ng-click="save()" ng-model="data.val">Memuaskan</ion-radio>' +
        '<ion-radio ng-value=5 ng-click="save()" ng-model="data.val">Amat Memuaskan</ion-radio>';

      return customTemplate;
    }

    this.getAnswerMark = function(index) {
      var response = ['Amat Tidak Memuaskan', 'Tidak Memuaskan', 'Tidak Pasti', 'Memuaskan', 'Amat Memuaskan'];
      return response[index - 1];
    }

    this.separateQuestion = function(data) {
      var bhg = 'Bahagian ';
      var bhgA = [],
        bhgB = [],
        bhgC = [],
        bhgD = [],
        bhgE = [],
        bhgF = [],
        bhgG = [];
      for (var i = 0; i < data.length; i++) {
        if (data[i].name.includes(bhg + "A")) {
          bhgA.push(data[i]);
        } else if (data[i].name.includes(bhg + "B")) {
          bhgB.push(data[i]);
        } else if (data[i].name.includes(bhg + "C")) {
          bhgC.push(data[i]);
        } else if (data[i].name.includes(bhg + "D")) {
          bhgD.push(data[i]);
        } else if (data[i].name.includes(bhg + "E")) {
          bhgE.push(data[i]);
        } else if (data[i].name.includes(bhg + "F")) {
          bhgF.push(data[i]);
        } else if (data[i].name.includes(bhg + "G")) {
          bhgG.push(data[i]);
        }
      }
      soalanAFactory.setData(bhgA);
      soalanBFactory.setData(bhgB);
      soalanCFactory.setData(bhgC);
      soalanDFactory.setData(bhgD);
      soalanEFactory.setData(bhgE);
      soalanFFactory.setData(bhgF);
      soalanGFactory.setData(bhgG);
    }

    this.separateResponseLookup = function(data) {
      responseAFactory.setData(data);
    }

    this.separateResponse = function(data) {
      var bhg = 'Bahagian ';
      var value = { button: 'Mula', text: 'Sila Pastikan Jawab Kesemua Soalan Sebelum Keluar Dari Applikasi MADA' };
      if (data) {
        for (var i = 0; i < data.length; i++) {
          if (data[i].name.includes(bhg + "A")) {

            var set = {
              ques: data[i].question,
              quesId: data[i].question_id,
              ans: data[i].response,
              ansId: data[i].response_id,
            };

            marksAFactory.setData(set);
          } else if (data[i].name.includes(bhg + "B")) {
            marksBFactory.setData(data[i].ans, data[i].quesId);
          } else if (data[i].name.includes(bhg + "C")) {
            marksCFactory.setData(data[i].ans, data[i].quesId);
          } else if (data[i].name.includes(bhg + "D")) {
            marksDFactory.setData(data[i].ans, data[i].quesId);
          } else if (data[i].name.includes(bhg + "E")) {
            marksEFactory.setData(data[i].ans, data[i].quesId);
          } else if (data[i].name.includes(bhg + "F")) {
            marksFFactory.setData(data[i].ans, data[i].quesId);
          } else if (data[i].name.includes(bhg + "G")) {
            marksGFactory.setData(data[i].ans, data[i].quesId);
            value = { button: 'Lihat Statistik', text: 'Bancian Anda Telah Diterima dan Direkod. Untuk Melihat Bancian, Sila Sentuh "Lihat Statistik"' };
          }
        }
      }


      homeValue.set(value);
    }

  }
]);

app.service('httpService', ['$ionicPopup', '$http', 'API_URL', 'madaService', '$ionicLoading', 'localForageFactory', '$state', 'soalanLength', function($ionicPopup, $http, API_URL, madaService, $ionicLoading, localForageFactory, $state, soalanLength) {
  var session = this;
  this.getSoalan = function() {
    $http({
      method: 'GET',
      url: API_URL + "/getSoalan",
      headers: { 'Content-Type': 'application/json' }
    }).success(function(response) {
      madaService.separateQuestion(response);
      console.log(response.length);
      soalanLength.set(response.length);
      session.getResponseLookup();
    }).error(function(err) {
      $ionicLoading.hide();
      session.erroPopup();
      console.log(err);
    })
  }

  this.getResponseLookup = function() {
    $http({
      method: 'GET',
      url: API_URL + "/getResponseLookup",
      headers: { 'Content-Type': 'application/json' }
    }).success(function(response) {
      session.getResponse(response);
    }).error(function(err) {
      $ionicLoading.hide();
      console.log(err);
    })
  }

  this.sendResponse = function(data, page) {
    localForageFactory.get('response').then(
      function(promise) {
        if (promise) {
          for (var i = 0; i < data.length; i++) {
            for (var j = 0; j < promise.length; j++) {
              if (data[i].quesId == promise[j].quesId) {
                promise.splice(j, 1);
              }
            }
          }

          data = promise.concat(data);
        }

        localForageFactory.set('response', data);
        if (page) {
          if (page != "summary") {
            $state.go(page);
          } else {
            session.sendAllResponse(data, page);
          }
        }

      },
      function(err) {
        $ionicLoading.hide();
        console.error(err);
      }
    );

  }

  this.sendAllResponse = function(data, page) {
    $ionicLoading.show({ template: '<ion-spinner icon="ripple"></ion-spinner><p>Memuat Naik Maklumbalas..</p>' });
    localForageFactory.get("id").then(
      function(id) {
        $http({
          method: 'POST',
          url: API_URL + "/sendResponse/" + id,
          data: data,
          headers: { 'Content-Type': 'application/json' }
        }).success(function(response) {
          console.log(response);
          $ionicLoading.hide();
          if (page) {
            $state.go(page);
          }
        }).error(function(err) {
          $ionicLoading.hide();
          console.log(err);
        })
      },
      function(err) {
        $ionicLoading.hide();
        console.error(err);
      }
    );
  }

  this.getResponse = function(data) {
    localForageFactory.get("response").then(
      function(response) {
        madaService.separateResponseLookup(data);
        madaService.separateResponse(response);
 // navigator.splashscreen.hide();
        console.log("API Loaded...");
        $ionicLoading.hide();
      },
      function(err) {
        $ionicLoading.hide();
        console.error(err);
      }
    );


    // localForageFactory.get("id").then(
    //   function(id) {
    //     $http({
    //       method: 'GET',
    //       url: API_URL + "/getResponse/" + id,
    //       headers: { 'Content-Type': 'application/json' }
    //     }).success(function(response) {
    //       madaService.separateResponseLookup(data);
    //       madaService.separateResponse(response);
    //       console.log("API Loaded...");
    //       $ionicLoading.hide();
    //       //navigator.splashscreen.hide();
    //     }).error(function(err) {
    //       $ionicLoading.hide();
    //       console.log(err);
    //       //navigator.splashscreen.hide();
    //     })
    //   },
    //   function(err) {
    //     console.error(err);
    //   }
    // );

  }



  this.erroPopup = function() {
    var customTemplate = "<br/><p><center>Sila Pastikan Sambungan ke Internet Tersedia dan Tutup Aplikasi MADA</center></p><br/><br/>";

    var alertPopup = $ionicPopup.show({
      title: 'Tiada Sambungan ke Internet',
      template: customTemplate
    });
  }

}]);

angular.module('app.routes', [])

  .config(function($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider



      .state('home', {
        url: '/home',
        templateUrl: 'templates/home.html',
        controller: 'homeCtrl',
        resolve: {
          loadAfter: function($rootScope, $state, soalanLength, localForageFactory) {
            console.log();


            localForageFactory.get("response").then(
              function(response) {
                if (response) {
                  if (response.length < soalanLength.get()) {
                    $rootScope.$emit("booleanHome");
                  } else {
                    $rootScope.$emit("homeMethod");
                  }
                } else {
                  $rootScope.$emit("booleanHome");
                }
              },
              function(err) {
                $ionicLoading.hide();
                console.error(err);
              }
            );
          }
        }
      })

      .state('bhgA1', {
        url: '/bhgA1',
        templateUrl: 'templates/bhgA1.html',
        controller: 'bhgA1Ctrl'
      })

      .state('bhgA2', {
        url: '/bhgA2',
        templateUrl: 'templates/bhgA2.html',
        controller: 'bhgA2Ctrl'
      })

      .state('bhgA3', {
        url: '/bhgA3',
        templateUrl: 'templates/bhgA3.html',
        controller: 'bhgA3Ctrl'
      })

      .state('bhgA4', {
        url: '/bhgA4',
        templateUrl: 'templates/bhgA4.html',
        controller: 'bhgA4Ctrl'
      })

      .state('bhgA5', {
        url: '/bhgA5',
        templateUrl: 'templates/bhgA5.html',
        controller: 'bhgA5Ctrl'
      })

      .state('bhgB', {
        url: '/bhgB',
        templateUrl: 'templates/bhgB.html',
        controller: 'bhgBCtrl'
      })

      .state('bhgC', {
        url: '/bhgC',
        templateUrl: 'templates/bhgC.html',
        controller: 'bhgCCtrl'
      })

      .state('bhgD', {
        url: '/bhgD',
        templateUrl: 'templates/bhgD.html',
        controller: 'bhgDCtrl'
      })

      .state('bhgE', {
        url: '/bhgE',
        templateUrl: 'templates/bhgE.html',
        controller: 'bhgECtrl'
      })

      .state('bhgF', {
        url: '/bhgF',
        templateUrl: 'templates/bhgF.html',
        controller: 'bhgFCtrl'
      })

      .state('bhgG', {
        url: '/bhgG',
        templateUrl: 'templates/bhgG.html',
        controller: 'bhgGCtrl'
      })

      .state('summary', {
        url: '/summary',
        templateUrl: 'templates/summary.html',
        controller: 'summaryCtrl'
      })

      .state('chart', {
        url: '/chart',
        templateUrl: 'templates/chart.html',
        controller: 'chartCtrl'
      })

    $urlRouterProvider.otherwise('/home')



  });
